#include "OpenSenseMapConfig.h"

int EEPROM_START_POSITION_OSM = 70; // ssid = 32 + passwd = 32 + 3 = OK

char openSenseMapBoxId[26] = "";
char openSenseMapTempId[26] = "";
char openSenseMapHumidityId[26] = "";
char openSenseMapPressureId[26] = "";
char openSenseMapA1Id[26] = "";
char openSenseMapA2Id[26] = "";
char openSenseMapA3Id[26] = "";
char openSenseMapA4Id[26] = "";

void loadOpenSenseMapSettings() {
  EEPROM.begin(512);
  EEPROM.get(EEPROM_START_POSITION_OSM, openSenseMapBoxId);
  EEPROM.get(EEPROM_START_POSITION_OSM+sizeof(openSenseMapBoxId) * 1, openSenseMapTempId);
  EEPROM.get(EEPROM_START_POSITION_OSM+sizeof(openSenseMapBoxId) * 2, openSenseMapHumidityId);
  EEPROM.get(EEPROM_START_POSITION_OSM+sizeof(openSenseMapBoxId) * 3, openSenseMapPressureId);
  EEPROM.get(EEPROM_START_POSITION_OSM+sizeof(openSenseMapBoxId) * 4, openSenseMapA1Id);
  EEPROM.get(EEPROM_START_POSITION_OSM+sizeof(openSenseMapBoxId) * 5, openSenseMapA2Id);
  EEPROM.get(EEPROM_START_POSITION_OSM+sizeof(openSenseMapBoxId) * 6, openSenseMapA3Id);
  EEPROM.get(EEPROM_START_POSITION_OSM+sizeof(openSenseMapBoxId) * 7, openSenseMapA4Id);
  EEPROM.end();

  if (strlen(openSenseMapBoxId) != 24) {
    openSenseMapBoxId[0] = 0;
    openSenseMapTempId[0] = 0;
    openSenseMapHumidityId[0] = 0;
    openSenseMapPressureId[0] = 0;
    openSenseMapA1Id[0] = 0;
    openSenseMapA2Id[0] = 0;
    openSenseMapA3Id[0] = 0;
    openSenseMapA4Id[0] = 0;
  }

  Serial.println("Recovered OSM settings:");
  Serial.println(strlen(openSenseMapBoxId)>0?openSenseMapBoxId: "<no openSenseMapBoxId>");
  Serial.println(strlen(openSenseMapTempId)>0?openSenseMapTempId: "<no openSenseMapTempId>");
  Serial.println(strlen(openSenseMapHumidityId)>0?openSenseMapHumidityId: "<no openSenseMapHumidityId>");
  Serial.println(strlen(openSenseMapPressureId)>0?openSenseMapPressureId: "<no openSenseMapPressureId>");
  Serial.println(strlen(openSenseMapA1Id)>0?openSenseMapA1Id: "<no openSenseMapA1Id>");
  Serial.println(strlen(openSenseMapA2Id)>0?openSenseMapA2Id: "<no openSenseMapA2Id>");
  Serial.println(strlen(openSenseMapA3Id)>0?openSenseMapA3Id: "<no openSenseMapA3Id>");
  Serial.println(strlen(openSenseMapA4Id)>0?openSenseMapA4Id: "<no openSenseMapA4Id>");
}


void saveOpenSenseMapSettings(String senseBoxId, String senseBoxTemperatureId, String senseBoxHumidityId, String senseBoxPressureId, String analog1,  String analog2,  String analog3,  String analog4) {
  senseBoxId.toCharArray(openSenseMapBoxId, sizeof(openSenseMapBoxId) - 1);
  senseBoxTemperatureId.toCharArray(openSenseMapTempId, sizeof(openSenseMapTempId) - 1);
  senseBoxHumidityId.toCharArray(openSenseMapHumidityId, sizeof(openSenseMapHumidityId) - 1);
  senseBoxPressureId.toCharArray(openSenseMapPressureId, sizeof(openSenseMapPressureId) - 1);

  analog1.toCharArray(openSenseMapA1Id, sizeof(openSenseMapA1Id) - 1);
  analog2.toCharArray(openSenseMapA2Id, sizeof(openSenseMapA2Id) - 1);
  analog3.toCharArray(openSenseMapA3Id, sizeof(openSenseMapA3Id) - 1);
  analog4.toCharArray(openSenseMapA4Id, sizeof(openSenseMapA4Id) - 1);

  Serial.println("saving ...");
  Serial.println("Box id: " + senseBoxId);
  Serial.println("temperature id: " + senseBoxTemperatureId);
  Serial.println("humidity id: " + senseBoxHumidityId);
  Serial.println("pressure id: " + senseBoxPressureId);

  Serial.println("a1 id: " + analog1);
  Serial.println("a2 id: " + analog2);
  Serial.println("a3 id: " + analog3);
  Serial.println("a4 id: " + analog4);
  

  EEPROM.begin(512);

  EEPROM.put(EEPROM_START_POSITION_OSM, openSenseMapBoxId);
  EEPROM.put(EEPROM_START_POSITION_OSM+sizeof(openSenseMapBoxId) * 1, openSenseMapTempId);
  EEPROM.put(EEPROM_START_POSITION_OSM+sizeof(openSenseMapBoxId) * 2, openSenseMapHumidityId);
  EEPROM.put(EEPROM_START_POSITION_OSM+sizeof(openSenseMapBoxId) * 3, openSenseMapPressureId);

  EEPROM.put(EEPROM_START_POSITION_OSM+sizeof(openSenseMapBoxId) * 4, openSenseMapA1Id);
  EEPROM.put(EEPROM_START_POSITION_OSM+sizeof(openSenseMapBoxId) * 5, openSenseMapA2Id);
  EEPROM.put(EEPROM_START_POSITION_OSM+sizeof(openSenseMapBoxId) * 6, openSenseMapA3Id);
  EEPROM.put(EEPROM_START_POSITION_OSM+sizeof(openSenseMapBoxId) * 7, openSenseMapA4Id);

  EEPROM.commit();
  EEPROM.end();

}

String getOpenSenseBoxId() {
  return String(openSenseMapBoxId);
}

String getOpenSenseTemperatureId() {
  return String(openSenseMapTempId);
}

String getOpenSenseHumidityId() {
  return String(openSenseMapHumidityId);
}

String getOpenSensePressureId() {
  return String(openSenseMapPressureId);
}

String getOpenSenseA1Id() {
  return String(openSenseMapA1Id);
}

String getOpenSenseA2Id() {
  return String(openSenseMapA2Id);
}

String getOpenSenseA3Id() {
  return String(openSenseMapA3Id);
}

String getOpenSenseA4Id() {
  return String(openSenseMapA4Id);
}
