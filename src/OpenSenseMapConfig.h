#ifndef OPENSENSEMAP_CONFIG_H
#define OPENSENSEMAP_CONFIG_H

#include <Arduino.h>
#include <String.h>
#include <EEPROM.h>

void loadOpenSenseMapSettings();

void saveOpenSenseMapSettings(String senseBoxId, String senseBoxTemperatureId, String senseBoxHumidityId, String senseBoxPressureId, String analog1,  String analog2,  String analog3,  String analog4);

String getOpenSenseBoxId();
String getOpenSenseTemperatureId();
String getOpenSenseHumidityId();
String getOpenSensePressureId();

String getOpenSenseA1Id();
String getOpenSenseA2Id();
String getOpenSenseA3Id();
String getOpenSenseA4Id();

#endif
