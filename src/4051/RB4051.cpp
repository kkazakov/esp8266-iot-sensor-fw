#include <Arduino.h>
#include "RB4051.h"

RB4051::RB4051(int s0, int s1) {
    m_s0 = s0;
    m_s1 = s1;
    pinMode(m_s0, OUTPUT);
    pinMode(m_s1, OUTPUT);
    m_currentPin = 0;
}

void RB4051::setInput(int inputToSet) {
    if ((inputToSet >= 0) && (inputToSet < 4)) {
        m_currentPin = inputToSet;
        int m_r0 = bitRead(m_currentPin, 0);
        int m_r1 = bitRead(m_currentPin, 1);
        digitalWrite(m_s0, m_r0);
        digitalWrite(m_s1, m_r1);
    }
}

int RB4051::getCurrentInput() {
    return m_currentPin;
}

int RB4051::RAAnalogRead() {
    return analogRead(m_readPin);
}