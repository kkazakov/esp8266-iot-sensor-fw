#define RB4051_h
#ifdef RB4051_h

/*
Simple object for controlling a 4051 multiplexer.  This just does the writes
and keeps track of the pin that's set on the 4051.

NOTE, NOTE! Original library found here:

https://github.com/hippymulehead/RA4051


I've just tweaked it a bit to suit the simpleness of this project.

*/

class RB4051 {
    public:
        // Init the object and set the pins to use for s0-s1
        RB4051(int s0, int s1);
        // Set the multiplexer pin to "pinToSet"
        void setInput(int inputToSet);
        // Get what pin is currently set
        int getCurrentInput();
        // Analog demux read
        int RAAnalogRead();
    protected:
        int             m_s0;
        int             m_s1;
        int             m_currentPin;
        int             m_readPin;
    private:
};
#endif