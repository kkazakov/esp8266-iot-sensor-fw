#ifndef SENSOR_H
#define SENSOR_H

#define HAS_BME280 true // set to false for BMP180

#define HAS_4051 true // if it has a multiplexer for analog inputs

#include <Arduino.h>
#include <String.h>
#include <Wire.h>

#if HAS_BME280

  #include <Adafruit_Sensor.h>
  #include <Adafruit_BME280.h>

  #define BME280_ADDRESS (0x76)

#else

  #include <Adafruit_BMP085.h>

#endif

#if HAS_4051
  #include "4051/RB4051.h"
#endif

#include "Settings.h"
#include "OpenSenseMap/OpenSenseMap.h"
#include "ThingSpeak/ThingSpeak.h"
#include "WebServer.h"
#include "Wifi.h"
#include "OpenSenseMapConfig.h"
#include "ThingSpeakConfig.h"

void measure();
void measureAndSend();
void sleep(unsigned long ms);
void deepsleep(unsigned long micros);

float getTemperature();
float getPressure();
float getAltitude();

#if HAS_4051
int getA1();
int getA2();
int getA3();
int getA4();
#endif

#if HAS_BME280
float getHumidity();
#endif

#endif
